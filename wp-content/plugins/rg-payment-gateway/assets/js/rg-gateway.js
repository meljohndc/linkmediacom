function rg_resize_iframe(obj) {
    obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
}

function rg_check_submit( form ){
	var elements 		= form.elements;
	var submit_token 	= '';
	var error_elem 		= document.getElementById( 'rg_ErrorMessage' );

	for( var i = 0; i < elements.length; i++ ){
		//console.log( elements[i].id );

		if( elements[i].id == 'RocketGateToken' ){
			submit_token = elements[i].value;
		}
	}
	
	var post_data 	= {
		'action' 		: 'rg_gateway_request',
		'submit_token' 	: submit_token,
	};

	jQuery.ajax({
		method: 'POST',
		url: parent.rg_url.ajax_url,
		data: post_data,
		success : function( res ){
			
			if( res.result == true ){

				for (const property in res) {
					/*console.log(property + '-----' + res[property] );*/
					parent.rg_fill_hidden_value('rg_result_' + property, res[property] );
				}

				setTimeout( function(){
					parent.rg_place_order_click();
				}, 1000);
			}else{
				error_elem.innerHTML = res.reason_text;
				error_elem.style.display = 'block';
			}
		}
	});
}

function rg_submit_func( obj ){
	
	RocketGateSetSubmitCB( rg_check_submit );

	return false;
}