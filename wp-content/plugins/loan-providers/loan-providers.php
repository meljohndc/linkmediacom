<?php
/*
Plugin Name: Loan Providers
Description: Custom post for the list of loan providers
*/
/* Start Adding Functions Below this Line */
// custom post type function
function create_posttype() {

    register_post_type( 'loan_providers',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Loan Providers' ),
                'singular_name' => __( 'Loan Provider' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'loan_providers'),
            'show_in_rest' => true,

        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

/*
* Creating a function to create our CPT
*/

function custom_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Loan Providers', 'Post Type General Name', 'cash-bay-child' ),
        'singular_name'       => _x( 'Loan Provider', 'Post Type Singular Name', 'cash-bay-child' ),
        'menu_name'           => __( 'Loan Providers', 'cash-bay-child' ),
        'parent_item_colon'   => __( 'Parent Loan Provider', 'cash-bay-child' ),
        'all_items'           => __( 'All Loan Providers', 'cash-bay-child' ),
        'view_item'           => __( 'View Loan Provider', 'cash-bay-child' ),
        'add_new_item'        => __( 'Add New Loan Provider', 'cash-bay-child' ),
        'add_new'             => __( 'Add New', 'cash-bay-child' ),
        'edit_item'           => __( 'Edit Loan Provider', 'cash-bay-child' ),
        'update_item'         => __( 'Update Loan Provider', 'cash-bay-child' ),
        'search_items'        => __( 'Search Loan Provider', 'cash-bay-child' ),
        'not_found'           => __( 'Not Found', 'cash-bay-child' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'cash-bay-child' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'loan-providers', 'cash-bay-child' ),
        'description'         => __( 'Loan Provider news and reviews', 'cash-bay-child' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        // 'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' 		  => true,
		'menu_icon'            => 'dashicons-video-alt',
// 		'register_meta_box_cb' => 'wpt_add_event_metaboxes',

    );

    // Registering your Custom Post Type
    register_post_type( 'loan_providers', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_post_type', 0 );
add_action( 'add_meta_boxes', 'add_loan_metaboxes' );

/**
 * Adds a metabox to the right side of the screen under the Publish box
 */
function add_loan_metaboxes() {
	add_meta_box(
		'loan_info',
		'Loan Information',
		'loan_info',
		'loan_providers',
		'side',
		'default'
	);
}
/**
 * Output the HTML for the metabox.
 */
function loan_info() {
	global $post;

	// Nonce field to validate form request came from current site
	wp_nonce_field( basename( __FILE__ ), 'loan_fields' );

	// Get Value of Fields From Database
	$loanAmountMin = get_post_meta( $post->ID, 'loanAmountMin', true );
	$loanAmountMax = get_post_meta( $post->ID, 'loanAmountMax', true );
	$loanTermMin = get_post_meta( $post->ID, 'loanTermMin', true );
	$loanTermMax = get_post_meta( $post->ID, 'loanTermMax', true );
	$interestRate = get_post_meta( $post->ID, 'interestRate', true );
	$minApr = get_post_meta( $post->ID, 'minApr', true );
	$maxApr = get_post_meta( $post->ID, 'maxApr', true );
	$responseTime = get_post_meta( $post->ID, 'responseTime', true );
	$minimumAge = get_post_meta( $post->ID, 'minimumAge', true );
	$specialLabel = get_post_meta( $post->ID, 'specialLabel', true );
	$trustScore = get_post_meta( $post->ID, 'trustScore', true );
	$companyUrl = get_post_meta( $post->ID, 'companyUrl', true );


	// Output the field
  ?>
  <div class="row">
    <div class="lp-label">Loan Amount Min</div>
    <div class="lp-fields"><input type="text" name="loanAmountMin" value="<?php echo $loanAmountMin; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Loan Amount Max</div>
    <div class="lp-fields"><input type="text" name="loanAmountMax" value="<?php echo $loanAmountMax; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Loan Term Min</div>
    <div class="lp-fields"><input type="text" name="loanTermMin" value="<?php echo $loanTermMin; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Loan Term Max</div>
    <div class="lp-fields"><input type="text" name="loanTermMax" value="<?php echo $loanTermMax; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Interest Rate</div>
    <div class="lp-fields"><input type="text" name="interestRate" value="<?php echo $interestRate; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Min APR</div>
    <div class="lp-fields"><input type="text" name="minApr" value="<?php echo $minApr; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Max APR</div>
    <div class="lp-fields"><input type="text" name="maxApr" value="<?php echo $maxApr; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Response Time</div>
    <div class="lp-fields"><input type="text" name="responseTime" value="<?php echo $responseTime; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Minimum Age</div>
    <div class="lp-fields"><input type="text" name="minimumAge" value="<?php echo $minimumAge; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Special Label</div>
    <div class="lp-fields"><input type="text" name="specialLabel" value="<?php echo $specialLabel; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">Trust Score</div>
    <div class="lp-fields"><input type="text" name="trustScore" value="<?php echo $trustScore; ?>"></div>
  </div>
  <br/>
  <div class="row">
    <div class="lp-label">URL</div>
    <div class="lp-fields"><input type="text" name="companyUrl" value="<?php echo $companyUrl; ?>"></div>
  </div>
  <?php
}

//  Save these multiple fields value in the Database
function save_loan_fields_metabox(){

    global $post;


    if(isset($_POST["loanAmountMin"])) :
    update_post_meta($post->ID, 'loanAmountMin', intval($_POST["loanAmountMin"]));
    endif;
    if(isset($_POST["loanAmountMax"])) :
    update_post_meta($post->ID, 'loanAmountMax', intval($_POST["loanAmountMax"]));
    endif;
    if(isset($_POST["loanTermMin"])) :
    update_post_meta($post->ID, 'loanTermMin', intval($_POST["loanTermMin"]));
    endif;
    if(isset($_POST["loanTermMax"])) :
    update_post_meta($post->ID, 'loanTermMax', intval($_POST["loanTermMax"]));
    endif;
    if(isset($_POST["interestRate"])) :
    update_post_meta($post->ID, 'interestRate', floatval($_POST["interestRate"]));
    endif;
    if(isset($_POST["minApr"])) :
    update_post_meta($post->ID, 'minApr', floatval($_POST["minApr"]));
    endif;
    if(isset($_POST["maxApr"])) :
    update_post_meta($post->ID, 'maxApr', floatval($_POST["maxApr"]));
    endif;
    if(isset($_POST["responseTime"])) :
    update_post_meta($post->ID, 'responseTime', $_POST["responseTime"]);
    endif;
    if(isset($_POST["minimumAge"])) :
    update_post_meta($post->ID, 'minimumAge', intval($_POST["minimumAge"]));
    endif;
    if(isset($_POST["specialLabel"])) :
    update_post_meta($post->ID, 'specialLabel', $_POST["specialLabel"]);
    endif;
    if(isset($_POST["trustScore"])) :
    update_post_meta($post->ID, 'trustScore', floatval($_POST["trustScore"]));
    endif;
    if(isset($_POST["companyUrl"])) :
    update_post_meta($post->ID, 'companyUrl', $_POST["companyUrl"]);
    endif;

}
add_action('save_post', 'save_loan_fields_metabox');

//  Add style for the meta boxes
function lp_load_plugin_css() {
  ?>
  <style>
    .row{
      padding:15px 0;
      clear:both;
    }
    .lp-label {
      float:left;
      width: 25%;
    }
    .lp-fields{
      float:left;
      width: 75%;
    }
    .lp-fields input{
        width: 100%;
    }
    /* Mobile style */
    @media (max-width: 1023px) {
      .lp-label,
      .lp-fields {
        width: 100%;
      }
    }
  </style>
  <?php
}
add_action( 'admin_enqueue_scripts', 'lp_load_plugin_css' );

/* Stop Adding Functions Below this Line */
?>
