<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package borrow
 */
global $borrow_option;
get_header(); ?>

<div class="content-wrap">
  <div class="<?php if(is_user_logged_in()){ echo'container-fluid';}else{echo 'container';} ?>">
    <div class="row flex-column-reverse flex-md-row">
      <?php if(is_user_logged_in()){ ?>
      <div class="content-side-wrap col-xl-2 col-lg-3 col-md-4">
        <div class="content-side">
          <?php get_sidebar();?>
        </div>
      </div>
      <?php } ?>
      <div class="content-body-wrap <?php if(is_user_logged_in()){ echo'col-xl-10 col-lg-9 col-md-8';}else{echo 'col-12';} ?> pt-4">
        <div class="row">
          <div class="col-md-12 content-head">
            <div class="page-breadcrumb">
              <?php if($borrow_option['bread-switch']==true){ ?>
                <ol class="breadcrumb">
                    <?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </ol>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="pinside30">
              <!-- main container -->
              <!-- content begin -->
   <div class="single-blog">
      

        <div class="row">
          <div class="col-md-12">
            <div class="content-body">
              <div class="row">
                <div class="col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-md-12">
                    <div class="post-holder">
                    <div class="post-block mb40">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="post-img mb30">
                            <?php if(get_the_post_thumbnail()){ ?>              
                                <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="">
                            <?php } ?>
                          </div>
                        <div class="post-content">
                          <h1><?php the_title(); ?></h1>
                          
                          <?php the_content(); ?>
                          <?php
                            if(get_the_tag_list()) {
                                echo get_the_tag_list('<ul class="tag-list"><li>','</li><li>','</li></ul>');
                            }
                          ?>
                        </div>
                  </div>
                </div>
              </div>
            </div>	
            <div class="post-related mb40">
              <div class="row">
                <div class="col-md-12">
                  <h2 class="post-related-title mb20"><?php echo esc_html_e('Related post','borrow'); ?></h2>
                </div>
              </div>
              <div class="row">
                <?php
                  $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 2, 'post__not_in' => array($post->ID) ) );
                  if( $related ) foreach( $related as $post ) 
                  {
                  setup_postdata($post); ?>

                  <div class="col-md-4 mb30 col-sm-6 col-xs-6 col-6">
                    <div class="related-img mb20"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail('medium', array('class' => 'img-responsive')); ?></a></div>
                    <div class="post-related-content">
                      <h4><a href="<?php the_permalink() ?>" class="title"><?php the_title(); ?></a></h4>
                      <?php if(has_category()) { ?>
                        <span> <?php esc_html_e('in','borrow'); ?> 
                          "<?php the_category(' , ', ' '); ?>"
                        </span>
                      <?php } ?>
                    </div>	        
                  </div>   
                  <?php }
                  wp_reset_postdata(); ?>
              </div>
            </div>	
					
				  <?php endwhile; // End of the loop. ?>
          </div> 

          
        </div>
      
    </div>
    </div>
    </div>
    </div>
    <!-- content close -->
	

              <!-- content close -->
            </div>
          </div>
        </div>
       
      </div>
    </div>
  </div>
</div>


	<!-- subheader close -->
	
<?php get_footer(); ?>
