<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package borrow
 */
global $borrow_option; ?>
<div class="footer section-space80">
    <!-- footer -->
  <?php if(isset($borrow_option['footer-select-pages']) and $borrow_option['footer-select-pages'] != "" ){              
        $about_id = $borrow_option['footer-select-pages'];
        $about_page = get_post($about_id);
        echo do_shortcode( $about_page->post_content );
  }else{ ?>
  <div class="container">
    <div class="row">
      <?php get_sidebar('footer'); ?>
    </div>
  </div>
  <?php } ?>
</div>
<div class="tiny-footer">
  <!-- tiny footer -->
  <div class="container">
      <div class="row">
          <?php if($borrow_option['footer_ltext']!=''){ ?>
          <div class="col-md-6 col-sm-6 col-xs-6">
              <p><?php echo wp_kses( $borrow_option['footer_ltext'], wp_kses_allowed_html('post') ); ?></p>
          </div>
          <?php } ?>
          <?php if($borrow_option['footer_rtext']!=''){ ?>
          <div class="col-md-6 col-sm-6 text-right col-xs-6">
              <p><?php echo wp_kses( $borrow_option['footer_rtext'], wp_kses_allowed_html('post') ); ?></p>
          </div>
          <?php } ?>
      </div>
  </div>
</div>

<?php if(isset($borrow_option['theme_layout']) and $borrow_option['theme_layout']=="boxed_version" ){ ?>
</div><!-- Close .boxed-wrapper -->
<?php } ?>

<a id="to-the-top"><i class="fa fa-angle-up"></i></a> 
<?php wp_footer(); ?>
</body>
</html>
