<?php
/**
 * Template Name: No Header & Footer
 */
get_header(); ?>

	<!-- content begin -->
<div class=""><!-- main container -->
  <div class="container">
    <div class="row min-vh-100 align-items-center">
      <div class="col-md-12">
        <div class="bg-white pinside40">
          <div class="row">
            <div class="col-sm-12 col-xs-12">
              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php while ( have_posts() ) : the_post(); ?>
                	
        					<?php the_content(); ?>
        					
        				<?php endwhile; // End of the loop. ?>
              </div>                   
            </div>
          </div>
        </div>
	    </div>
    </div>
  </div>
</div>
<!-- content close -->
<style>
.header, .footer{display:none;}
</style>

<?php get_footer(); ?>


