<?php
// Conditional Nav Menu

function wpc_wp_nav_menu_args( $args = '' ) {
   if( is_user_logged_in()) { 
      if( 'primary' == $args['theme_location'] ) { 
         $args['menu'] = 'logged-in';
      }
   } else { 
      if( 'primary' == $args['theme_location'] ) { 
         $args['menu'] = 'logged-out';
      }
   } 
   return $args;
}
add_filter( 'wp_nav_menu_args', 'wpc_wp_nav_menu_args' );


//Limit 1 item in cart
add_filter( 'woocommerce_add_to_cart_validation', 'wc_only_one_in_cart', 99, 2 ); 
function wc_only_one_in_cart( $passed, $added_product_id ) {
   wc_empty_cart();
   return $passed;
}

// Redirect to checkout page
add_filter( 'woocommerce_add_to_cart_redirect', 'wploans_redirect_checkout_add_cart' );
function wploans_redirect_checkout_add_cart() {
   return home_url( 'checkout' );
}

// custom redirection
add_action("template_redirect", 'wploans_redirection');
function wploans_redirection(){
   // Redirect to homepage if cart is empty
   global $woocommerce;
   if( is_cart() ){
      wp_safe_redirect( site_url() );
   }
   // redirect club
   if ( is_product() ) {
      global $post;
      if ('loans subscription' == $post->post_name) {
        wp_safe_redirect( site_url() );
      }
   }
   // redirect shop
   if( is_shop() ){
        wp_safe_redirect( site_url() );
   }
}

// Removes cart notices from the checkout page
add_filter( 'wc_add_to_cart_message_html', '__return_null' );
// Removes cart notices from the checkout page
function sv_remove_cart_notice_on_checkout() {
	if ( function_exists( 'wc_cart_notices' ) ) {
		remove_action( 'woocommerce_before_checkout_form', array( wc_cart_notices(), 'add_cart_notice' ) );
	}
}
add_action( 'init', 'sv_remove_cart_notice_on_checkout' );


// Remove My Account Menus
add_filter ( 'woocommerce_account_menu_items', 'misha_remove_my_account_links' );
function misha_remove_my_account_links( $menu_links ){
 
	unset( $menu_links['edit-address'] ); // Addresses
	unset( $menu_links['payment-methods'] ); // Remove Payment Methods
 
	return $menu_links;
 
}
// Removes the "My Memberships" table from my account area
function sv_remove_my_memberships_table() {
   if ( function_exists( 'wc_memberships' ) && ! is_admin() ) {
	  remove_filter( 'woocommerce_account_menu_items', array( wc_memberships()->get_frontend_instance()->get_members_area_instance(), 'add_account_members_area_menu_item' ), 999 );
	}
}
add_action( 'init', 'sv_remove_my_memberships_table', 50 );
// Remove the "Change Payment Method" button from the My Subscriptions table.
function eg_remove_my_subscriptions_button( $actions, $subscription ) {

	foreach ( $actions as $action_key => $action ) {
		switch ( $action_key ) {
			case 'change_payment_method':	// Hide "Change Payment Method" button
				unset( $actions[ $action_key ] );
				break;
			default:
				error_log( '-- $action = ' . print_r( $action, true ) );
				break;
		}
	}

	return $actions;
}
add_filter( 'wcs_view_subscription_actions', 'eg_remove_my_subscriptions_button', 100, 2 );


// Billing fields customization
add_filter( 'woocommerce_billing_fields', 'custom_billing_fields_filter', 100);
function custom_billing_fields_filter( $address_fields ) {
	$address_fields['billing_first_name']['required']   = true;
	$address_fields['billing_last_name']['required']    = true;
	$address_fields['billing_email']['required']        = true;
    $address_fields['billing_address_2']['placeholder'] = 'Additional Address';
    $address_fields['billing_postcode'] = array(
        'label' => 'Postcode/Zip',
        'placeholder' => 'Postcode/Zip',
        'required' => true,
        'class' => array('required-field-override')
    );
    $address_fields['billing_address_1'] = array(
        'label' => 'Street Address',
        'placeholder' => 'Street Address',
        'required' => true,
        'class' => array('required-field-override')
    );
    $address_fields['billing_city'] = array(
        'label' => 'Town/City',
        'placeholder' => 'Town/City',
        'required' => true,
        'class' => array('required-field-override')
    );
	unset($address_fields['billing_company']);
	unset($address_fields['billing_country']);
	unset($address_fields['billing_state']);
    return $address_fields;
}


// load JS after jquery
add_action('wp_enqueue_scripts', 'shopper_load_enqueue_custom_js');
function shopper_load_enqueue_custom_js() {
//     wp_enqueue_script('main', get_stylesheet_directory_uri().'/inc/assets/js/main.js');
    if(is_page( 'checkout' )){
//       wp_enqueue_script('jquery-3.4.0', get_stylesheet_directory_uri().'/inc/assets/js/jquery-3.4.0.min.js');
      wp_enqueue_script('qtip', get_stylesheet_directory_uri().'/inc/assets/js/jquery.qtip.min.js');
      wp_enqueue_script('parsley', get_stylesheet_directory_uri().'/inc/assets/js/parsley.js');
      wp_enqueue_script('validate_error_messages', get_stylesheet_directory_uri().'/inc/assets/js/validate_error_messages.js');
      wp_enqueue_script('validate', get_stylesheet_directory_uri().'/inc/assets/js/validate.js');
      wp_register_style('customcssparent', get_stylesheet_directory_uri() . '/inc/assets/css/qtip.min.css');
      wp_enqueue_style('customcssparent');
    }
}
add_action( 'wp_print_scripts', 'my_deregister_javascript', 100 );
function my_deregister_javascript() {
   if(is_page( 'checkout' )){
    wp_deregister_script( 'borrow-isotope' );
    }
}
/**
 * Add WooCommerce Checkbox checkout
 */
function bt_add_checkout_checkbox() {
   
    woocommerce_form_field( 'checkout-checkbox', array( // CSS ID
       'type'          => 'checkbox',
       'class'         => array('form-row mycheckbox'), // CSS Class
       'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
       'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
       'required'      => true, // Mandatory or Optional
       'label'         => 'I agree to register and try annaloan.com platform for 1 GBP and acknowledge that if not cancelled within due time, my membership will be automatically renewed at the price of 34.99 GBP and next membership charge will take place on first day of the next month. The charge will appear on your card statement as ANNALOAN.COM+441279969865', // Label and Link
    ));    
}
add_action( 'woocommerce_review_order_before_submit', 'bt_add_checkout_checkbox', 10 );


/**
 * Alert if checkbox not checked
 */ 
function bt_add_checkout_checkbox_warning() {
    if ( ! (int) isset( $_POST['checkout-checkbox'] ) ) {
        wc_add_notice( __( 'Please acknowledge the Checkbox' ), 'error' );
    }
}
add_action( 'woocommerce_checkout_process', 'bt_add_checkout_checkbox_warning' );

/**
 * Shop customizer
 */
require get_theme_file_path( '/inc/customizer.php' );


?>

