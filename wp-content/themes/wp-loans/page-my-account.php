<?php
/**
 * Template Name: My Account
 */

get_header(); ?>


	<!-- content begin -->
<div class="content-wrap"><!-- main container -->
  <div class="<?php if(is_user_logged_in()){ echo'container-fluid';}else{echo 'container min-vh';} ?>">
    <div class="row flex-column-reverse flex-md-row <?php if (! is_user_logged_in()){echo 'login-form-wrap';}?>">
      <?php if(is_user_logged_in()){?>
        <div class="content-side-wrap col-xl-2 col-lg-3 col-md-4">
          <div class="content-side">
            <?php get_sidebar();?>
          </div>
        </div>
      <?php }?>
      <div class="content-body-wrap <?php if(is_user_logged_in()) {echo 'col-lg-9 col-md-8 pt-4';} else{echo 'col-md-7 col-lg-6 col-xl-5';}?> pt-4">
        <div class="bg-white pinside40">
          <div class="row">
          <div class="col-12">
              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php while ( have_posts() ) : the_post(); ?>
                	
        					<?php the_content(); ?>
        					<?php if (! is_user_logged_in()){?>
                    <div class="text-center">
                      <a href="<?php echo home_url(); ?>" class=" btn-link">Return to homepage</a>
                    </div>
                  <?php }?>
        				<?php endwhile; // End of the loop. ?>
              </div>                   
            </div>

                        
          </div>
        </div>
	    </div>
    </div>
  </div>
</div>
<!-- content close -->
<?php if(! is_user_logged_in()) { // hide header & footer if user is not logged in?>
<style>
  #sticky-wrapper, .header, .footer{display:none;}
</style>
<?php }?>

<?php get_footer(); ?>
