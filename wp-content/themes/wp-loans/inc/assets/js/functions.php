<?php
// Conditional Nav Menu

function wpc_wp_nav_menu_args( $args = '' ) {
   if( is_user_logged_in()) { 
      if( 'primary' == $args['theme_location'] ) { 
         $args['menu'] = 'logged-in';
      }
   } else { 
      if( 'primary' == $args['theme_location'] ) { 
         $args['menu'] = 'logged-out';
      }
   } 
   return $args;
}
add_filter( 'wp_nav_menu_args', 'wpc_wp_nav_menu_args' );


//Limit 1 item in cart
add_filter( 'woocommerce_add_to_cart_validation', 'wc_only_one_in_cart', 99, 2 ); 
function wc_only_one_in_cart( $passed, $added_product_id ) {
   wc_empty_cart();
   return $passed;
}

// Redirect to checkout page
add_filter( 'woocommerce_add_to_cart_redirect', 'wploans_redirect_checkout_add_cart' );
function wploans_redirect_checkout_add_cart() {
   return home_url( 'checkout' );
}

// custom redirection
add_action("template_redirect", 'wploans_redirection');
function wploans_redirection(){
   // Redirect to homepage if cart is empty
   global $woocommerce;
   if( is_cart() ){
      wp_safe_redirect( site_url() );
   }
   // redirect club
   if ( is_product() ) {
      global $post;
      if ('loans subscription' == $post->post_name) {
        wp_safe_redirect( site_url() );
      }
   }
   // redirect shop
   if( is_shop() ){
        wp_safe_redirect( site_url() );
   }
}

// Removes cart notices from the checkout page
add_filter( 'wc_add_to_cart_message_html', '__return_null' );
// Removes cart notices from the checkout page
function sv_remove_cart_notice_on_checkout() {
	if ( function_exists( 'wc_cart_notices' ) ) {
		remove_action( 'woocommerce_before_checkout_form', array( wc_cart_notices(), 'add_cart_notice' ) );
	}
}
add_action( 'init', 'sv_remove_cart_notice_on_checkout' );


// Remove My Account Menus
add_filter ( 'woocommerce_account_menu_items', 'misha_remove_my_account_links' );
function misha_remove_my_account_links( $menu_links ){
 
	unset( $menu_links['edit-address'] ); // Addresses
	unset( $menu_links['payment-methods'] ); // Remove Payment Methods
 
	return $menu_links;
 
}
// Removes the "My Memberships" table from my account area
function sv_remove_my_memberships_table() {
   if ( function_exists( 'wc_memberships' ) && ! is_admin() ) {
	  remove_filter( 'woocommerce_account_menu_items', array( wc_memberships()->get_frontend_instance()->get_members_area_instance(), 'add_account_members_area_menu_item' ), 999 );
	}
}
add_action( 'init', 'sv_remove_my_memberships_table', 50 );
// Remove the "Change Payment Method" button from the My Subscriptions table.
function eg_remove_my_subscriptions_button( $actions, $subscription ) {

	foreach ( $actions as $action_key => $action ) {
		switch ( $action_key ) {
			case 'change_payment_method':	// Hide "Change Payment Method" button
				unset( $actions[ $action_key ] );
				break;
			default:
				error_log( '-- $action = ' . print_r( $action, true ) );
				break;
		}
	}

	return $actions;
}
add_filter( 'wcs_view_subscription_actions', 'eg_remove_my_subscriptions_button', 100, 2 );

// Add custom registration fields
function wooc_extra_register_fields() {?>
  <div class="form-row">
    <div class="col-md-6">
       <input type="text" class="input-text form-control" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" placeholder="<?php _e( 'First name', 'woocommerce' ); ?>" required/>
        <span class="invalid-tooltip">This field is required</span>
    </div>
    <div class="col-md-6">
      <input type="text" class="input-text form-control" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" placeholder="<?php _e( 'Last name', 'woocommerce' ); ?>" required/>
    <span class="invalid-tooltip">This field is required</span>
    </div>
  </div>

  <div class="form-row">
    <div class="col-md-6">
       <input type="email" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" placeholder="<?php esc_html_e( 'Email address', 'woocommerce' ); ?>" required/><?php // @codingStandardsIgnoreLine ?>
        <span class="invalid-tooltip">This field is required</span>
    </div>
    <div class="col-md-6">
      <input type="text" class="input-text form-control" name="billing_phone" id="reg_billing_phone" value="<?php esc_attr_e( $_POST['billing_phone'] ); ?>" placeholder="Phone" required/>
    <span class="invalid-tooltip">This field is required</span>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-12">
       <input type="text" class="input-text form-control" name="billing_address_1" id="reg_billing_address_1" value="<?php esc_attr_e( $_POST['billing_address_1'] ); ?>" placeholder="Street Address" required/>
         <span class="invalid-tooltip">This field is required</span>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-12">
      <input type="text" class="input-text form-control" name="billing_address_2" id="reg_billing_address_2" value="<?php esc_attr_e( $_POST['billing_address_2'] ); ?>" placeholder="Additional Address"/>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
        <input type="text" class="input-text form-control" name="billing_postcode" id="reg_billing_postcode" value="<?php esc_attr_e( $_POST['billing_postcode'] ); ?>" placeholder="Postcode/Zip" required/>
        <span class="invalid-tooltip">This field is required</span>
    </div>
    <div class="col-md-6">
        <input type="text" class="input-text form-control" name="billing_city" id="reg_billing_city" value="<?php esc_attr_e( $_POST['billing_city'] ); ?>" placeholder="Town/City" required/>
        <span class="invalid-tooltip">This field is required</span>
    </div>
  </div>
<div class="clear"></div>
 
 <?php
 }
 // save registration fields to database
  function wooc_save_extra_register_fields( $customer_id ) {
        if ( isset( $_POST['billing_phone'] ) )
        {
           update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
        }
        if ( isset( $_POST['billing_email'] ) )
        {
           update_user_meta( $customer_id, 'billing_email', sanitize_text_field( $_POST['billing_email'] ) );
        }
        if ( isset( $_POST['billing_first_name'] ) )
        {
           update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
           update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
        }
        if ( isset( $_POST['billing_last_name'] ) )
        {
           update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
           update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
        }
        if ( isset( $_POST['billing_postcode'] ) )
        {
           update_user_meta( $customer_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
        }
        if ( isset( $_POST['billing_city'] ) )
        {
           update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
        }
        if ( isset( $_POST['billing_address_1'] ) )
        {
           update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
        }
        if ( isset( $_POST['billing_address_2'] ) )
        {
           update_user_meta( $customer_id, 'billing_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );
        }
  }
  add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );
  add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

// Validating custom registeration fields
 add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );
 function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
       if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
          $validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
       }
       if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
          $validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
       }
//        if ( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) ) {
//           $validation_errors->add( 'billing_postcode_error', __( '<strong>Error</strong>: Postcode/Zip code is required!.', 'woocommerce' ) );
//        }
      return $validation_errors;
 }

// Billing fields customization
add_filter( 'woocommerce_billing_fields', 'custom_billing_fields_filter', 100);
function custom_billing_fields_filter( $address_fields ) {
	$address_fields['billing_first_name']['required']   = true;
	$address_fields['billing_last_name']['required']    = true;
	$address_fields['billing_email']['required']        = true;
    $address_fields['billing_address_2']['placeholder'] = 'Additional Address';
    $address_fields['billing_postcode'] = array(
        'label' => 'Postcode/Zip',
        'placeholder' => 'Postcode/Zip',
        'required' => true,
        'class' => array('required-field-override')
    );
    $address_fields['billing_address_1'] = array(
        'label' => 'Street Address',
        'placeholder' => 'Street Address',
        'required' => true,
        'class' => array('required-field-override')
    );
    $address_fields['billing_city'] = array(
        'label' => 'Town/City',
        'placeholder' => 'Town/City',
        'required' => true,
        'class' => array('required-field-override')
    );
	unset($address_fields['billing_company']);
	unset($address_fields['billing_country']);
	unset($address_fields['billing_state']);
    return $address_fields;
}

// Remove checkout fields


// rename checkout button
add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' );
function woo_custom_order_button_text() {
    return __( 'Proceed to payment', 'woocommerce' );
}
// set flag to confirm details
add_action('woocommerce_after_checkout_validation', 'add_validation_flag');
function add_validation_flag($posted) {
    if ($_POST['confirm-order-flag'] == "1") {
        wc_add_notice( __( "custom_notice", 'validation_flag' ), 'error');
    }
}
// move checkout payment forms
add_action( 'after_setup_theme', 'move_checkout_payment_form' );
function move_checkout_payment_form() {
  remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
  add_action( 'woocommerce_after_order_notes', 'woocommerce_checkout_payment', 20 );
}

// load JS after jquery
add_action('wp_enqueue_scripts', 'shopper_load_enqueue_custom_js');
function shopper_load_enqueue_custom_js() {
    wp_enqueue_script('main', get_stylesheet_directory_uri().'/inc/assets/js/main.js');
    if(is_page( 'checkout' )){
      wp_enqueue_script('qtip', get_stylesheet_directory_uri().'/inc/assets/js/jquery-3.4.0.min.js');
      wp_enqueue_script('qtip', get_stylesheet_directory_uri().'/inc/assets/js/jquery.qtip.min.js');
      wp_enqueue_script('parsley', get_stylesheet_directory_uri().'/inc/assets/js/parsley.js');
      wp_enqueue_script('validate_error_messages', get_stylesheet_directory_uri().'/inc/assets/js/validate_error_messages.js');
      wp_enqueue_script('validate', get_stylesheet_directory_uri().'/inc/assets/js/validate.js');
    }
}


/**
 * Shop customizer
 */
require get_theme_file_path( '/inc/customizer.php' );


?>

