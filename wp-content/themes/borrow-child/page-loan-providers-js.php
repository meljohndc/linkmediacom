<?php
/**
 * Template Name: loan-providers-js
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package borrow
 */

global $borrow_option;
get_header(); ?>

<div class="content-wrap">
  <div class="<?php if(is_user_logged_in()){ echo'container-fluid';}else{echo 'container';} ?>">
    <div class="row flex-column-reverse flex-md-row">
      <?php if(is_user_logged_in()){ ?>
      <div class="content-side-wrap col-xl-2 col-lg-3 col-md-4">
        <div class="content-side">
          <?php get_sidebar();?>
        </div>
      </div>
      <?php } ?>
      <div class="content-body-wrap <?php if(is_user_logged_in()){ echo'col-xl-10 col-lg-9 col-md-8';}else{echo 'col-12';} ?> pt-4">
        <div class="row">
          <div class="col-md-12 content-head">
            <div class="page-breadcrumb">
              <?php if($borrow_option['bread-switch']==true){ ?>
                <ol class="breadcrumb">
                    <?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </ol>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="bg-white pinside30">
              <!-- main container -->
              <div class="row">
                <div class="col-md-12">
                  <div class="content-body">
                    <div class="row">
                      <!-- content begin -->
                        <div class="<?php if(is_user_logged_in()){ echo'col-lg-6';}else{echo 'col-12';} ?>">
                          <?php
                            while (have_posts()) : the_post();
                                the_content();
                            endwhile;
                          ?>
                        </div>
                        <?php
                        if(is_user_logged_in()){  
                          /**
                          * Get field values
                          * Replace fieldnames with changing the calculated field forms in this page
                          */
                          /**
                          * Setup query the loan providers
                          */
                          $args = array(
                              'post_type' => 'loan_providers',
                              'post_status' => 'publish',
                              'posts_per_page' => -1,
                              'meta_key' => 'trustScore',
                              'orderby' => 'meta_value_num',
                              'order' => 'DESC'
                          );
                          
                          $loop = new WP_Query( $args );
                          ?>
                          <div class="col-lg-6">
                            <div id="top-provider"></div>
                          </div>
                          <div id="providers-list" class="col-12">
                            <script>
                              const allLoanProviders = [
                                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                  {
                                    "providerName" : "<?php echo the_title(); ?>",
                                    "providerImg" : '<?php has_post_thumbnail() ? the_post_thumbnail("medium") : " " ;?>',
                                    "loanAmountMin" : <?php echo get_post_meta($post->ID, 'loanAmountMin', true);?>,
                                    "loanAmountMax" : <?php echo get_post_meta($post->ID, 'loanAmountMax', true); ?>,
                                    "loanTermMin" : <?php echo get_post_meta($post->ID, 'loanTermMin', true); ?>,
                                    "loanTermMax" : <?php echo get_post_meta($post->ID, 'loanTermMax', true); ?>,
                                    "interestRate" : <?php echo get_post_meta($post->ID, 'interestRate', true); ?>,
                                    "minApr" : <?php echo get_post_meta($post->ID, 'minApr', true); ?>,
                                    "maxApr" : <?php echo get_post_meta($post->ID, 'maxApr', true); ?>,
                                    "responseTime" : "<?php echo get_post_meta($post->ID, 'responseTime', true); ?>",
                                    "minimumAge" : <?php echo get_post_meta($post->ID, 'minimumAge', true); ?>,
                                    "specialLabel" : "<?php echo get_post_meta($post->ID, 'specialLabel', true); ?>",
                                    "trustScore" : <?php echo get_post_meta($post->ID, 'trustScore', true); ?>,
                                    "companyUrl" : "<?php echo get_post_meta($post->ID, 'companyUrl', true); ?>"
                                  },
                                <?php endwhile; ?>
                              ]
                              jQuery(document).ready(function() {
                                var loanProviders = [],
                                providersList = '',
                                topProvider = '',
                                wpCurrency = '<?php echo get_woocommerce_currency_symbol(); ?>';
                                loanProviders = allLoanProviders;
                                getTopProvider();
                                document.getElementById('top-provider').innerHTML = topProvider;
                                listProviders();  
                                document.getElementById('providers-wrap').innerHTML = providersList;
                                

                                setTimeout(() => {
                                  jQuery(".ui-slider").on("slidestop", function(event, ui) {
                                    let age = parseInt(document.getElementById("fieldname1_1").value),
                                    repayTime = parseInt(document.getElementById("fieldname3_1").value),
                                    loanSize = parseInt(document.getElementById("fieldname2_1").value);
                                    getProvidersByCriteria(age, repayTime, loanSize);

                                    getTopProvider();
                                    document.getElementById('top-provider').innerHTML = topProvider;
                                    listProviders();
                                    document.getElementById('providers-wrap').innerHTML = providersList;
                                    
                                  });
                                }, 2000);
                                  

                                
                                
                                function getProvidersByCriteria(age, repayTime, loanSize) {
                                  loanProviders = allLoanProviders.filter(item =>
                                      (parseInt(item.minimumAge) <= age) &&
                                      (parseInt(item.loanTermMin) <= repayTime) &&
                                      (parseInt(item.loanTermMax) >= repayTime) &&
                                      (parseInt(item.loanAmountMin) <= loanSize) &&
                                      (parseInt(item.loanAmountMax) >= loanSize)
                                  );
                                  return loanProviders;
                                }
                                function listProviders(){
                                  providersList = '';
                                  for (let provider of loanProviders){
                                    providersList += "<div class='provider-wrap card mb-2'>"
                                    providersList += "  <div class='row bg-white m-0 py-3'>"
                                    providersList += "    <div class='col-lg-2 col-6 order-0'>"
                                    providersList += "      <div class='provider-img'>" + provider.providerImg + "</div>";
                                    providersList += "    </div>"
                                    providersList += "    <div class='col-xl-2 col-lg-10 col-6 order-xl-2 text-md-right'>"
                                    providersList += "      <div class='provider-url'><a href='" + provider.companyUrl + "' class='btn btn-primary' target='_blank'>Apply Now</a></div>";
                                    providersList += "    </div>"
                                    providersList += "    <div class='col-xl-8 order-xl-1'>"
                                    providersList += "      <h5 class='provider-name'>" + provider.providerName + "</h5>";
                                    providersList += "      <div class='row'>"
                                    providersList += "        <div class='col-lg-3 col-md-6'>"
                                    providersList += "          <div class='amt-range'><label>Loan Amount:</label>" + wpCurrency + provider.loanAmountMin.toLocaleString();
                                    providersList += "          - " + wpCurrency + provider.loanAmountMax.toLocaleString() + "</div>";
                                    providersList += "        </div>"
                                    providersList += "        <div class='col-lg-3 col-md-6'>"
                                    providersList += "          <div class='term-range'><label>Loan Term:</label>" + provider.loanTermMin;
                                    providersList += "          - " + provider.loanTermMax + " months</div>";
                                    providersList += "        </div>"
                                    providersList += "        <div class='col-lg-3 col-md-6'>"
                                    providersList += "          <div class='interest-rate'><label>Interest Rate:</label>" + provider.interestRate + "%</div>";
                                    providersList += "        </div>"
                                    providersList += "        <div class='col-lg-3 col-md-6'>"
                                    providersList += "          <div class='apr-range'><label>APR:</label>" + provider.minApr;
                                    providersList += "          - " + provider.maxApr + "%</div>";
                                    providersList += "        </div>"
                                    providersList += "      </div>"
                                    providersList += "    </div>"
                                    providersList += "  </div>"
                                    providersList += "  <div class='row p-3 justify-content-between'>"
                                    providersList += "    <div class='col'><div class='float-left'>"
                                    providersList += "      <div class='rating-stars' style='--rating: " + provider.trustScore  + ";'></div>"
                                    providersList += "      <div class='trust-score'><span>Trust Score: </span>" + provider.trustScore + "</div>";
                                    providersList += "    </div></div>"
                                    providersList += "    <div class='col text-right'>"
                                    providersList += "      <div class='response-time'><b>Response Time: </b>" + provider.responseTime + "</div>";
                                    providersList += "      <div class='min-age'><b>Minimum Age: </b>" + provider.minimumAge + " years</div>";
                                    providersList += "    </div>"
                                    providersList += "  </div>"
                                    providersList += "</div>"
                                    providersList += "<div class='provider-note small mb-4'>" + provider.specialLabel + "</div>";
                                  }
                                  return providersList;
                                }
                                function getTopProvider(){
                                  topProvider = '';
                                  for (let provider of loanProviders){
                                    topProvider += "<div class='provider-wrap card mb-2'>"
                                    topProvider += "  <div class='row bg-white m-0 py-3'>"
                                    topProvider += "    <div class='col-6'>"
                                    topProvider += "      <div class='provider-img'>" + provider.providerImg + "</div>";
                                    topProvider += "    </div>"
                                    topProvider += "    <div class='col-6 text-right'>"
                                    topProvider += "      <div class='provider-url'><a href='" + provider.companyUrl + "' class='btn btn-primary' target='_blank'>Apply Now</a></div>";
                                    topProvider += "    </div>"
                                    topProvider += "  </div>"
                                    topProvider += "  <div class='row bg-white m-0 py-3 border-top'>"
                                    topProvider += "    <h5 class='col-12 provider-name'>" + provider.providerName + "</h5>";
                                    topProvider += "    <div class='col-12'>"
                                    topProvider += "      <div class='amt-range d-flex'><label>Loan Amount:</label><span>" + wpCurrency + provider.loanAmountMin.toLocaleString();
                                    topProvider += "        - " + wpCurrency + provider.loanAmountMax.toLocaleString() + "</span></div>";
                                    topProvider += "      </div>"
                                    topProvider += "      <div class='col-12'>"
                                    topProvider += "        <div class='term-range d-flex'><label>Loan Term:</label><span>" + provider.loanTermMin;
                                    topProvider += "        - " + provider.loanTermMax + " months</span></div>";
                                    topProvider += "      </div>"
                                    topProvider += "      <div class='col-12'>"
                                    topProvider += "        <div class='interest-rate d-flex'><label>Interest Rate:</label><span>" + provider.interestRate + "%</span></div>";
                                    topProvider += "      </div>"
                                    topProvider += "      <div class='col-12'>"
                                    topProvider += "        <div class='apr-range d-flex'><label>APR:</label><span>" + provider.minApr;
                                    topProvider += "        - " + provider.maxApr + "%</span></div>";
                                    topProvider += "      </div>"
                                    topProvider += "  </div>"
                                    topProvider += "  <div class='row p-3 justify-content-between'>"
                                    topProvider += "    <div class='col'><div class='float-left'>"
                                    topProvider += "      <div class='rating-stars' style='--rating: " + provider.trustScore  + ";'></div>"
                                    topProvider += "      <div class='trust-score'><span>Trust Score: </span>" + provider.trustScore + "</div>";
                                    topProvider += "    </div></div>"
                                    topProvider += "    <div class='col text-right'>"
                                    topProvider += "      <div class='response-time'><b>Response Time: </b>" + provider.responseTime + "</div>";
                                    topProvider += "      <div class='min-age'><b>Minimum Age: </b>" + provider.minimumAge + " years</div>";
                                    topProvider += "    </div>"
                                    topProvider += "  </div>"
                                    topProvider += "</div>"
                                    topProvider += "<div class='provider-note small mb-4'>" + provider.specialLabel + "</div>";
                                    break;
                                  }
                                  if(topProvider==''){
                                    topProvider = "<div class='card p-3 bg-white'><h3>NO SUITABLE LOAN PROVIDER FOUND.</h3> <p>Update your criteria and find the loan provider that is best for you</p>";
                                  }
                                  return topProvider;
                                }
                              });
                            </script>
                            <div id="providers-wrap">
                            </div>
                          </div>
                        <?php } ?>
                        <?php wp_reset_postdata();?>
                    </div>
                  </div>
                </div>
              </div>
              <!-- content close -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
