<?php
/**
 * Template Name: Checkout Page
 */
get_header(); ?>

	<!-- content begin -->
<div class=""><!-- main container -->
  <div class="container min-vh">
    <div class="row align-items-center">
      <div class="col-md-12">
        <div class="bg-white pinside40 mb-3">
          <div class="row">
            <div class="col-sm-12 col-xs-12">
              <h1>Personal Details</h1>
              <form id="contact-form" method="post" class="contactForm" onsubmit="document.charset = 'ISO-8859-1'"
          accept-charset="ISO-8859-1" enctype="application/x-www-form-urlencoded;charset=ISO-8859-1"
          data-parsley-validate method="post" data-qtip-classes="qtip-custom" action="../payment">
                <div class="row marginbot-10">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" type="text" name="firstname" placeholder="First Name" data-parsley-name
                        required maxlength="20" minlength="2" data-parsley-trigger="blur" data-tooltip-at="top center"
                        data-tooltip-my="bottom center">
                      <!-- <div class="validation"></div> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" type="text" name="lastname" placeholder="Last Name" data-parsley-name
                        required maxlength="20" minlength="2" data-parsley-trigger="blur" data-tooltip-at="top center"
                        data-tooltip-my="bottom center" />
                      <!-- <div class="validation"></div> -->
                    </div>
                  </div>
                </div>
                <div class="row marginbot-20">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" type="email" name="email" placeholder="Email" data-parsley-type="email"
                        required data-parsley-trigger="blur" data-tooltip-at="top center"
                        data-tooltip-my="bottom center" />
                      <!-- <div class="validation"></div> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" type="tel" name="phone" placeholder="Phone" data-parsley-phone required
                        data-parsley-trigger="blur" data-tooltip-at="top center" data-tooltip-my="bottom center" />
                      <!-- <div class="validation"></div> -->
                    </div>
                  </div>
                </div>
                <div class="row marginbot-10">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" type="text" name="address1" placeholder="Street Address"
                        data-parsley-address required maxlength="80" minlength="2" data-parsley-trigger="blur"
                        data-tooltip-at="top center" data-tooltip-my="bottom center" />
                      <!-- <div class="validation"></div> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" type="text" name="address2" placeholder="Additional Address"/>
                      <!-- <div class="validation"></div> -->
                    </div>
                  </div>
                </div>
                <div class="row marginbot-10 mb-4">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" type="text" name="zipcode" placeholder="Zip Code" data-parsley-zip="en"
                        required data-parsley-trigger="blur" data-tooltip-at="top center"
                        data-tooltip-my="bottom center" />
                      <!-- <div class="validation"></div> -->
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" type="text" name="city" placeholder="City" data-parsley-name required
                        maxlength="40" minlength="2" data-parsley-trigger="blur" data-tooltip-at="top center"
                        data-tooltip-my="bottom center" />
                      <!-- <div class="validation"></div> -->
                    </div>
                  </div>
                </div>
                <div class="text-center mt-4">
                  <div><button type="submit" class="btn btn-primary mb-4" id="btnSignup" name="btnSignup">Proceed to Payment</button></div>
                  <a href="../" class="btn-link">Return to homepage</a>
                </div>
                
              </form>
              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php while ( have_posts() ) : the_post(); ?>
                	
        					<?php the_content(); ?>
        					
        				<?php endwhile; // End of the loop. ?>
              </div>                   
            </div>
          </div>
        </div>
        <div class="text-center">
          <?php echo do_shortcode('[tags_company_name]'); ?>,
          Registration #<?php echo do_shortcode('[tags_shop_registration]'); ?>,
          <?php echo do_shortcode('[tags_shop_address_local]'); ?>,
          <?php echo do_shortcode('[tags_shop_address_city]'); ?>,
          <?php echo do_shortcode('[tags_shop_address_postcode]'); ?>,
          <?php echo do_shortcode('[tags_shop_address_country]'); ?>,
          <?php echo do_shortcode('[tags_shop_phone]'); ?>
        </div>
	    </div>
    </div>
  </div>
</div>
<!-- content close -->
<style>
#sticky-wrapper, .header, .footer{display:none;}
/*Below is the styling for the validator*/
.qtip-custom {
    background-color: #E05263;
    border-color:#E05263;
    border-width: 1px;
    color:#ffffff;
}
input.parsley-error, 
input:not([type="radio"]).parsley-error, select.parsley-error {
    color: #E05263;
    background-color: #F8E9E9 !important;
    border: 1px solid #E05263 !important;
    -webkit-text-fill-color: #E05263 !important;
}

/*This style is required for parsley*/
.parsley-errors-list {
    margin: 0;
    padding: 0;
    list-style-type: none;
    opacity: 0;
    font-size: 0;
    display:none !important;
}

</style>
<?php get_footer(); ?>


