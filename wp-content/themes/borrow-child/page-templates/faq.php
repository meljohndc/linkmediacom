<?php
/**
* Template Name: FAQ
 *
 * @package borrow
 */
global $borrow_option;
get_header(); ?>

<div class="content-wrap">
  <div class="<?php if(is_user_logged_in()){ echo'container-fluid';}else{echo 'container';} ?>">
    <div class="row flex-column-reverse flex-md-row">
      <?php if(is_user_logged_in()){ ?>
      <div class="content-side-wrap col-xl-2 col-lg-3 col-md-4">
        <div class="content-side">
          <?php get_sidebar();?>
        </div>
      </div>
      <?php } ?>
      <div class="<?php if(is_user_logged_in()){ echo'content-body-wrap col-xl-10 col-lg-9 col-md-8';}else{echo 'col-12';} ?> pt-4">
        <div class="row">
          <div class="col-md-12 content-head">
            <div class="page-breadcrumb">
              <?php if($borrow_option['bread-switch']==true){ ?>
                <ol class="breadcrumb">
                    <?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </ol>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="bg-white pinside30">
              <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                  <h1 class="page-title">
                    <?php the_title(); ?>
                  </h1>
                </div>
              </div>
               <!-- main container -->
              <div class="row">
                <div class="col-md-12">
                  <div class="content-body">
                    <?php
                      while (have_posts()) : the_post();
                          the_content();
                      endwhile;
                    ?>
                    <!-- faq start -->
                    <div id="faq">
                      <div class="section">
                          <h6>What is <?php echo do_shortcode('[tags_shop_domain]' ); ?>?</h6>
                          <p>
                              <?php echo do_shortcode('[tags_shop_domain]' ); ?> is a transparent consumer loan aggregation platform. Our mission is to help you make a smarter decision when choosing a loan provider. Based on loan size, payback period, and age we provide a list of loan providers that
                              match the selected criteria. <?php echo do_shortcode('[tags_shop_domain]' ); ?> does not provide financial or other advice in relation to the products or services compared, nor does it provide a recommendation or endorsement. When it comes to loan providers listing,
                              <?php echo do_shortcode('[tags_shop_domain]' ); ?> offers impartial and independent comparison and results are ranked by variables such as Annual Percentage Rate (APR). <span><?php echo do_shortcode('[tags_shop_domain]' ); ?></span> is operated for commercial purposes.
                          </p>
                      </div>
                      <div class="section">
                          <h6>What are the coupons?</h6>
                          <p>
                              <?php echo do_shortcode('[tags_shop_domain]' ); ?> isn’t a store - instead, we’re here to help you find the best deals online. Because you’re never buying directly from us, we have no access to your order history. We only direct you to the website where the coupon is
                              applicable. Coupons are updated regularly for you to make smarter purchasing decisions.
                          </p>
                      </div>
                      <div class="section">
                          <h6>How do I contact customer support?</h6>
                          <p>
                              Our Customer Service is open 24 hours a day, 7 days a week and we guarantee all requests will be answered within 24 hours. Reach out to our customer support by email: <?php echo do_shortcode('[tags_shop_email]' ); ?> or by our customer support phone number:
                              <?php echo do_shortcode('[tags_shop_phone]' ); ?>
                          </p>
                      </div>
                      <div class="section">
                          <h6>How do I change my personal and contact information?</h6>
                          <p>You are able to change your personal details as well as your contact information simply by reaching out to our customer support via chat or email.</p>
                      </div>
                      <div class="section">
                          <h6>How do I change my password?</h6>
                          <p>In order to change your password, please reach out to our customer support via chat or email.</p>
                      </div>
                      <div class="section">
                          <h6>How do I cancel my subscription?</h6>
                          <p>For you to cancel your current subscription simply contact our Customer Support on Phone or Email. After canceling your membership you will still be able to use the membership service until the end of your subscription period.</p>
                      </div>
                      <div class="section">
                          <h6>Other requests? Reach out to us!</h6>
                          <p>We’d love to hear from you! Feel free to reach out to our customer support staff for further assistance!</p>
                      </div>
                    </div>
                    <!-- tc end -->
                  </div>
                </div>
              </div>
              <!-- content close -->
            </div>
          </div>
        </div>
       
      </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>
