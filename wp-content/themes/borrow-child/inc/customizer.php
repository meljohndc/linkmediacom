<?php
/**
 * WP Loans Theme Customizer
 * 
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function themeslug_sanitize_checkbox($checked)
{
    // Boolean check.
    return ((isset($checked) && true == $checked) ? true : false);
}

//Tags
function WP_Shopper_Starter_customize_register($wp_customize)
{
    //Shop tags
    $wp_customize->add_section('shop_tags', array(
        'title' => __('Shop Tags', 'wp-loans-child') ,
        'priority' => 30,
    ));
    // company name
    $wp_customize->add_setting('tags_company_name', array(
        'default' => __('Company Name', 'wp-loans-child') ,
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'tags_company_name', array(
        'label' => __('Company Name', 'wp-loans-child') ,
        'section' => 'shop_tags',
        'settings' => 'tags_company_name',
        'type' => 'text'
    )));

    // shop domain
    $wp_customize->add_setting('tags_shop_domain', array(
        'default' => __(get_site_url(), 'wp-loans-child') ,
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'tags_shop_domain', array(
        'label' => __('Domain', 'wp-loans-child') ,
        'section' => 'shop_tags',
        'settings' => 'tags_shop_domain',
        'type' => 'text'
    )));

    // shop email
    $wp_customize->add_setting('tags_shop_email', array(
        'default' => __('email@shopdomain.com', 'wp-loans-child') ,
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'tags_shop_email', array(
        'label' => __('Email Address', 'wp-loans-child') ,
        'section' => 'shop_tags',
        'settings' => 'tags_shop_email',
        'type' => 'text'
    )));

    // shop phone
    $wp_customize->add_setting('tags_shop_phone', array(
        'default' => __('+Dummyphone', 'wp-loans-child') ,
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'tags_shop_phone', array(
        'label' => __('Phone Number', 'wp-loans-child') ,
        'section' => 'shop_tags',
        'settings' => 'tags_shop_phone',
        'type' => 'text'
    )));

    // shop description
    $wp_customize->add_setting('tags_shop_desc', array(
        'default' => __('Description', 'wp-loans-child') ,
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'tags_shop_desc', array(
        'label' => __('Description', 'wp-loans-child') ,
        'section' => 'shop_tags',
        'settings' => 'tags_shop_desc',
        'type' => 'textarea'
    )));

    // shop registration
    $wp_customize->add_setting('tags_shop_registration', array(
        'default' => __('HE000000', 'wp-loans-child') ,
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'tags_shop_registration', array(
        'label' => __('Registration', 'wp-loans-child') ,
        'section' => 'shop_tags',
        'settings' => 'tags_shop_registration',
        'type' => 'text'
    )));

    // shop address country
    $wp_customize->add_setting('tags_shop_address_country', array(
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('tags_shop_address_country', array(
        'label' => __('Shop Address', 'wp-loans-child') ,
        'section' => 'shop_tags',
        'settings' => 'tags_shop_address_country',
        'input_attrs' => array(
            'placeholder' => __('Country', 'wp-loans-child') ,
        )
    ));

    // shop address state
    $wp_customize->add_setting('tags_shop_address_state', array(
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('tags_shop_address_state', array(
        'section' => 'shop_tags',
        'settings' => 'tags_shop_address_state',
        'input_attrs' => array(
            'placeholder' => __('State', 'wp-loans-child') ,
        )
    ));

    // shop address city
    $wp_customize->add_setting('tags_shop_address_city', array(
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('tags_shop_address_city', array(
        'section' => 'shop_tags',
        'settings' => 'tags_shop_address_city',
        'input_attrs' => array(
            'placeholder' => __('City', 'wp-loans-child') ,
        )
    ));

    // shop address local
    $wp_customize->add_setting('tags_shop_address_local', array(
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('tags_shop_address_local', array(
        'section' => 'shop_tags',
        'settings' => 'tags_shop_address_local',
        'type' => 'textarea',
        'input_attrs' => array(
            'placeholder' => __('Address Local', 'wp-loans-child') ,
        )
    ));

    // shop address postcode
    $wp_customize->add_setting('tags_shop_address_postcode', array(
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('tags_shop_address_postcode', array(
        'section' => 'shop_tags',
        'settings' => 'tags_shop_address_postcode',
        'type' => 'text',
        'input_attrs' => array(
            'placeholder' => __('Postcode', 'wp-loans-child') ,
        )
    ));

    // shop initial days
    $wp_customize->add_setting('tags_shop_initial_days', array(
        'default' => __('7', 'wp-shopper-starter') ,
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'tags_shop_initial_days', array(
        'label' => __('Initial Days', 'wp-shopper-starter') ,
        'section' => 'shop_tags',
        'settings' => 'tags_shop_initial_days',
        'type' => 'text',
        'input_attrs' => array(
            'placeholder' => __('Initial Days', 'wp-shopper-starter') ,
        )
    )));

    // shop currency
    $wp_customize->add_setting('tags_shop_currency', array(
        'default' => __(get_woocommerce_currency_symbol(), 'wp-shopper-starter') ,
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'tags_shop_currency', array(
        'label' => __('Currency', 'wp-shopper-starter') ,
        'section' => 'shop_tags',
        'settings' => 'tags_shop_currency',
        'type' => 'text',
        'input_attrs' => array(
            'placeholder' => __('Currency', 'wp-shopper-starter') ,
        )
    )));

    // initial price
    $wp_customize->add_setting('tags_shop_initial_price', array(
        'default' => __('1', 'wp-shopper-starter') ,
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'tags_shop_initial_price', array(
        'label' => __('Initial Price', 'wp-shopper-starter') ,
        'section' => 'shop_tags',
        'settings' => 'tags_shop_initial_price',
        'type' => 'text',
        'input_attrs' => array(
            'placeholder' => __('Initial Price', 'wp-shopper-starter') ,
        )
    )));

    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport = 'postMessage';
}
add_action('customize_register', 'WP_Shopper_Starter_customize_register');

function tags_company_namef()
{
    $i = get_theme_mod('tags_company_name');
    return $i;
}
function tags_shop_domainf()
{
    $i = get_theme_mod('tags_shop_domain');
    return $i;
}
function tags_shop_emailf()
{
    $i = get_theme_mod('tags_shop_email');
    return $i;
}
function tags_shop_phonef()
{
    $i = get_theme_mod('tags_shop_phone');
    return $i;
}
function tags_shop_descf()
{
    $i = get_theme_mod('tags_shop_desc');
    return $i;
}
function tags_shop_registrationf()
{
    $i = get_theme_mod('tags_shop_registration');
    return $i;
}
function tags_shop_address_countryf()
{
    $i = get_theme_mod('tags_shop_address_country');
    return $i;
}
function tags_shop_address_statef()
{
    $i = get_theme_mod('tags_shop_address_state');
    return $i;
}
function tags_shop_address_cityf()
{
    $i = get_theme_mod('tags_shop_address_city');
    return $i;
}
function tags_shop_address_localf()
{
    $i = get_theme_mod('tags_shop_address_local');
    return $i;
}
function tags_shop_address_postcodef()
{
    $i = get_theme_mod('tags_shop_address_postcode');
    return $i;
}
function tags_shop_initial_daysf()
{
    $i = get_theme_mod('tags_shop_initial_days');
    return $i;
}
function tags_shop_currencyf()
{
    $i = get_theme_mod('tags_shop_currency');
    return $i;
}
function tags_shop_initial_pricef()
{
    $i = get_theme_mod('tags_shop_initial_price');
    return $i;
}


//Shop tags shortcodes
add_shortcode('tags_company_name', 'tags_company_namef');
add_shortcode('tags_shop_domain', 'tags_shop_domainf');
add_shortcode('tags_shop_email', 'tags_shop_emailf');
add_shortcode('tags_shop_phone', 'tags_shop_phonef');
add_shortcode('tags_shop_desc', 'tags_shop_descf');
add_shortcode('tags_shop_registration', 'tags_shop_registrationf');
add_shortcode('tags_shop_address_country', 'tags_shop_address_countryf');
add_shortcode('tags_shop_address_state', 'tags_shop_address_statef');
add_shortcode('tags_shop_address_city', 'tags_shop_address_cityf');
add_shortcode('tags_shop_address_local', 'tags_shop_address_localf');
add_shortcode('tags_shop_address_postcode', 'tags_shop_address_postcodef');
add_shortcode('tags_shop_initial_days', 'tags_shop_initial_daysf');
add_shortcode('tags_shop_currency', 'tags_shop_currencyf');
add_shortcode('tags_shop_initial_price', 'tags_shop_initial_pricef');

