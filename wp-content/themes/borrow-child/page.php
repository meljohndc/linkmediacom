<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package borrow
 */
global $borrow_option;
get_header(); ?>

<div class="content-wrap">
  <div class="<?php if(is_user_logged_in()){ echo'container-fluid';}else{echo 'container';} ?>">
    <div class="row flex-column-reverse flex-md-row">
      <?php if(is_user_logged_in()){ ?>
      <div class="content-side-wrap col-xl-2 col-lg-3 col-md-4">
        <div class="content-side">
          <?php get_sidebar();?>
        </div>
      </div>
      <?php } ?>
      <div class="content-body-wrap <?php if(is_user_logged_in()){ echo'col-xl-10 col-lg-9 col-md-8';}else{echo 'col-12';} ?> pt-4">
        <div class="row">
          <div class="col-md-12 content-head">
            <div class="page-breadcrumb">
              <?php if($borrow_option['bread-switch']==true){ ?>
                <ol class="breadcrumb">
                    <?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </ol>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="bg-white pinside30">
              <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                  <h1 class="page-title">
                    <?php the_title(); ?>
                  </h1>
                </div>
              </div>
               <!-- main container -->
              <div class="row">
                <div class="col-md-12">
                  <div class="content-body">
                    <?php
                      while (have_posts()) : the_post();
                          the_content();
                      endwhile;
                    ?>
                  </div>
                </div>
              </div>
              <!-- content close -->
            </div>
          </div>
        </div>
       
      </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>
